export enum RoutePath {
    AUTH = 'auth',
    LOGIN = 'login',
    LOGIN_ABS = 'auth/login',
    SIGN_UP = 'signup',
    HOME = 'home',
    ADMIN = 'admin',
    LOGS_VIEW = 'logsView'
}