export enum ApiMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE"
}

export enum AuthEndPoints {
    LOGIN = "/login",
    LOGOUT = "/user/logout",
    SIGNUP = "/user",
    LOGIN_FORM = "/loginForm",
    SIGN_UP_FORM = "/signupForm"
}

export enum SimulationEndPoints {
    GET = "/simulations",
    GET_SIMULATION = "/simulation"
}