export interface Simulation {
    simulationRuns: SimulationRun[];
    scenarios: Scenario[];
}
export interface SimulationRun {
    startTime: number;
    endTime: number;
    scenarioId: string;
    carBuild: string;
    result: Result;
}

export interface Scenario {
    scenarioId: string;
    maxNumberOfStops: number;
    maxRunningTime: number;
}

export interface Result {
    numberOfStops: number;
    hasCollision: boolean;
}

export interface SimulationRunDataTableHeader {
    key: SortTableKey;
    value: string;
    sortType: SortTableType;
}
export interface SimulationRunDataTableRow {
    scenarioId: string;
    carBuild: string;
    startTime: number;
    runningTime: number;
    maxRunningTime: number;
    numberOfStops: number;
    maxNumberOfStops: number;
    hasCollision: boolean;
    doesScenarioPass: boolean;
}

export interface SimulationRunDataTableRow {
    scenarioId: string;
    carBuild: string;
    startTime: number;
    runningTime: number;
    maxRunningTime: number;
    numberOfStops: number;
    maxNumberOfStops: number;
    hasCollision: boolean;
    doesScenarioPass: boolean;
}

export interface SortSimulationTable {
    key: SortTableKey;
    type: SortTableType;
}

export interface FilterSimulationTable {
    scenario: string;
    carBuild: string;
}
export interface PaginationFormat  {
    numberOfPages: number;
    itemsPerPage: number;
    currentPage: number;
}

// Enums
export enum SortTableKey {
    SCENARIO = 'scenarioId',
    CAR_BUILD = 'carBuild',
    START_TIME = 'startTime',
    RUNNING_TIME = 'runningTime',
    NUMBER_OF_STOPS = 'numberOfStops',
    HAS_COLLISION = 'hasCollision',
    DOES_SCENARIOS_PASS = 'doesScenarioPass',
    NO_KEY = ''
}

export enum SortTableType {
    ASCENDENT = 'asc',
    DESCENDENT = 'desc'
}



