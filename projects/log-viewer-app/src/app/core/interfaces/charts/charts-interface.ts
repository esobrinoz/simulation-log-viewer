export interface DoughnutChartDataSource {
    key: string;
    val: number;
}
