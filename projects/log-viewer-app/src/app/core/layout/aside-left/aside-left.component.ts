import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, HostListener } from '@angular/core';

@Component({
  selector: 'eszsw-aside-left',
  templateUrl: './aside-left.component.html',
  styleUrls: ['./aside-left.component.scss']
})
export class AsideLeftComponent implements OnInit, AfterViewInit {
  
  @ViewChild('asideElement') asideElement: ElementRef;

  public imgPath: string;
  public wedSitePath: string;
  public linkedinPath: string;
  public gitLabPath: string;
  public webSiteLinkTitle:string;
  public linkedinLinkTitle:string;
  public gitLabTitle:string;
  public documentPath:string;
  public documentTitle:string;
  public conqualityTitle: string;
  public conqualityLink: string; 
  public jumIntoInterviewGameTitle: string;
  public jumIntoInterviewGameLink: string;

  constructor(private renderer2: Renderer2) { }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.fillFullHeight();
  }

  ngOnInit(): void {
   this.initialize();
  }

  ngAfterViewInit(): void {
    this.fillFullHeight();
  }

  public initialize(): void {
    this.imgPath = './assets/img/eszSoftwareLogo.png';
    this.wedSitePath = "https://eimardsobrinozurera.com/#/home";
    this.linkedinPath = "https://www.linkedin.com/in/eimardsobrinozurera/";
    this.gitLabPath = "https://gitlab.com/esobrinoz";
    this.webSiteLinkTitle = "Official Eimard's web site";
    this.linkedinLinkTitle = "Eimard's linkedin profile";
    this.gitLabTitle = "GitLab";
    this.documentPath = "/assets/pdf/LogViewerApp_documentation_EimardSobrino-Motional.pdf";
    this.documentTitle = "Project documentation";
    this.documentTitle = "Project documentation";
    this.conqualityTitle = "Download my app Conquality";
    this.conqualityLink = "https://play.google.com/store/apps/details?id=com.eszsoftware.conquality";
    this.jumIntoInterviewGameTitle = "Download JUMP into interview GAME";
    this.jumIntoInterviewGameLink = "https://play.google.com/store/apps/details?id=com.jumptipgame";
  }

  public fillFullHeight(): void {
    this.renderer2.setStyle(this.asideElement.nativeElement, 'height', window.innerHeight + 'px' );
  }

}
