import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'projects/log-viewer-app/src/environments/environment';
import { Observable } from 'rxjs';
import { ApiMethod, SimulationEndPoints } from '../../enums/endpoints';
import { Simulation } from '../../interfaces/simulations/simulations-interface';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class SimulationsService {

  constructor(private httpService: HttpService) { }

  public getSimulation(id:string): Observable<Simulation> {
    const url: string = `${SimulationEndPoints.GET}/${id}`;
    return this.httpService.requestCall(ApiMethod.GET, environment.apiUrl, url);
  }
  public getSimulations(): Observable<Simulation> {
    const params: HttpParams = new HttpParams().set('status', 'Send here your params');
    return this.httpService.requestCall(ApiMethod.GET, environment.apiUrl, SimulationEndPoints.GET, undefined, params);
  }
}
