import { RoutePath } from 'projects/log-viewer-app/src/app/core/enums/route.paths';
import { CompleteFormGuard } from './../core/guards/completeForm/complete-form.guard';
import { SignupComponent } from './pages/signup/signup.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ComponentsTag } from '../core/enums/component-tags';

const authRoutes: Routes = [
    {
      path: '',
      component: LoginComponent,
      data: { componentTag: ComponentsTag.LOGIN } 
    },
    {
      path: RoutePath.LOGIN,
      component: LoginComponent,
      data: { componentTag: ComponentsTag.LOGIN } 
    },
    {
      path: RoutePath.SIGN_UP,
      component: SignupComponent,
      canDeactivate:[CompleteFormGuard],
      data: { componentTag: ComponentsTag.SING_UP } 
    }
  ];


export const AUTH_ROUTES = RouterModule.forChild(authRoutes);
