import { Pipe, PipeTransform } from '@angular/core';
import { PaginationFormat, SimulationRunDataTableRow } from '../../core/interfaces/simulations/simulations-interface';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(array: SimulationRunDataTableRow[], pagination: PaginationFormat): SimulationRunDataTableRow[] {
    let sortedArray: SimulationRunDataTableRow[] = array;
    if (pagination && pagination.numberOfPages > 1 && array && array.length > 0) {
      const numberOfItemsSeen: number =  pagination.itemsPerPage * (pagination.currentPage);
      sortedArray = sortedArray.slice(numberOfItemsSeen, numberOfItemsSeen + pagination.itemsPerPage);
    }
    return sortedArray;
  }

}
