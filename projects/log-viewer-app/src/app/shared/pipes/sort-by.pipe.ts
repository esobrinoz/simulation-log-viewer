/*
 *ngFor="let c of oneDimArray | sortBy:'asc'"
 *ngFor="let c of arrayOfObjects | sortBy:'asc':'propertyName'"
*/
import { SimulationRunDataTableRow, SortSimulationTable, SortTableType } from './../../core/interfaces/simulations/simulations-interface';
import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {
  transform(array: SimulationRunDataTableRow[], sortTable?: SortSimulationTable): SimulationRunDataTableRow[] {
    let sortedArray: SimulationRunDataTableRow[] = array;
    if (sortTable && array && array.length > 0) {
      if (sortTable && !sortTable.key) { 
        sortedArray = (sortTable.type === SortTableType.ASCENDENT) ? array.sort() : array.sort().reverse();
      } else {
        sortedArray = orderBy(array, [sortTable.key], [sortTable.type]);
      } 
    }
    return sortedArray;
  }
}