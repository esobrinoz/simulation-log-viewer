import { FilterSimulationTable, SimulationRunDataTableRow } from './../../core/interfaces/simulations/simulations-interface';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchBy'
})
export class SearchPipe implements PipeTransform {

  transform(array: SimulationRunDataTableRow[], filters?: FilterSimulationTable): SimulationRunDataTableRow[] {
    let sortedArray: SimulationRunDataTableRow[] = array;
    if (filters && (filters.scenario !== '' || filters.carBuild !== '') && array && array.length > 0) {
      if (filters.scenario !== '') { 
        sortedArray = sortedArray.filter( (item: SimulationRunDataTableRow) => item.scenarioId.toLowerCase().indexOf(filters.scenario.toLowerCase()) > -1);
      } 
      if (filters.carBuild !== '') {
        sortedArray = sortedArray.filter( (item: SimulationRunDataTableRow) => item.carBuild.toLowerCase().indexOf(filters.carBuild.toLowerCase()) > -1);
      } 
    }
    return sortedArray;
  }

}
