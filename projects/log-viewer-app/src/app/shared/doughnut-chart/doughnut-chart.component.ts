import { PercentPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { DoughnutChartDataSource } from '../../core/interfaces/charts/charts-interface';

@Component({
  selector: 'eszsw-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit {
  public pipe: any = new PercentPipe("en-US");

  @Input() dataSource: DoughnutChartDataSource[]= [];
  @Input() chartTitle: string;
  @Input() percentageSearched: number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  customizeTooltip = (data: any) => {
    const tooltipData = {
        text: data.valueText + " simulation runs - " + this.pipe.transform(data.percent, "1.2-2")
    };
    return tooltipData;
  }

}
