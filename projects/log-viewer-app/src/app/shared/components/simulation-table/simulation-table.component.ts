import { PaginationPipe } from './../../pipes/pagination.pipe';
import { SortByPipe } from './../../pipes/sort-by.pipe';
import { SearchPipe } from './../../pipes/search.pipe';
import { SimulationRunDataTableHeader, SimulationRunDataTableRow, SortSimulationTable, SortTableKey, SortTableType, FilterSimulationTable, PaginationFormat } from './../../../core/interfaces/simulations/simulations-interface';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MyCustomPaginationComponent } from '../../../features/home/components/my-custom-pagination/my-custom-pagination.component';

@Component({
  selector: 'eszsw-simulation-table',
  templateUrl: './simulation-table.component.html',
  styleUrls: ['./simulation-table.component.scss'],
  providers: [SearchPipe, SortByPipe, PaginationPipe]
})
export class SimulationTableComponent implements OnInit {
  public sortTable: SortSimulationTable;
  public filteredArray:  SimulationRunDataTableRow[] = [];
  public resultsFound: number = 0;
  private _filters: FilterSimulationTable;
  @Input() set filters(updatedFilters: FilterSimulationTable) {
    this._filters = updatedFilters;
    this.filterArray();
  }
  get filters(): FilterSimulationTable {
      return this._filters;
  }

  private _rows:  SimulationRunDataTableRow[] = [];
  @Input() set rows(updatedRows:  SimulationRunDataTableRow[]) {
    this._rows = updatedRows;
    this.filterArray();
  }
  get rows():  SimulationRunDataTableRow[] {
      return this._rows;
  }
  @Input() headers: SimulationRunDataTableHeader[] = []; 
  constructor(private searchByPipe: SearchPipe, private sortByPipe: SortByPipe, private paginationPipe: PaginationPipe) { 
  }

  @ViewChild(MyCustomPaginationComponent) pagination: MyCustomPaginationComponent;

  ngOnInit(): void {
    this.setSortTable();
  }

  setSortTable(): void {
    this.sortTable = {
      key: SortTableKey.SCENARIO,
      type: SortTableType.ASCENDENT
    }
  }

  filterArray() {
    if (this.rows && this.rows.length > 0) {
      this.filterBySearch();
      this.sortArray();
      if (this.pagination) {
        this.pagination.setPagination(this.filteredArray);
      } 
    } else {
      this.filteredArray = [];
    }
  }

  filterBySearch() {
    if (this.filters) {
      this.filteredArray = this.searchByPipe.transform(this.rows, this.filters);
    } else {
      this.filteredArray = this.rows;
    }
    this.resultsFound = [...this.filteredArray].length;
  }

  changeSorting(header: SimulationRunDataTableHeader) {
    if (this.sortTable.key === header.key) {
      header.sortType = header.sortType === SortTableType.ASCENDENT ? SortTableType.DESCENDENT : SortTableType.ASCENDENT;
    } 
    this.sortTable =  {
      type : header.sortType,
      key: header.key
    };
    this.filterArray();
  }

  sortArray() {
    this.filteredArray = this.sortByPipe.transform(this.filteredArray, this.sortTable);
  }
  paginationFilter(paginationFormat: PaginationFormat) {
    this.filteredArray = this.paginationPipe.transform(this.filteredArray, paginationFormat);
  }
}
