import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core';

export abstract class BaseComponent implements OnDestroy {
    public subscriptions: Subscription[] = [];
    ngOnDestroy(): void {
        this.subscriptions.forEach( (subscription: Subscription) => subscription.unsubscribe());      
    }
}