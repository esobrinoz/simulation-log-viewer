import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCustomPaginationComponent } from './my-custom-pagination.component';

describe('MyCustomPaginationComponent', () => {
  let component: MyCustomPaginationComponent;
  let fixture: ComponentFixture<MyCustomPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCustomPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCustomPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
