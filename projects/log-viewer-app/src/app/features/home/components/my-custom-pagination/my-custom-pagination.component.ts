import { Component, Input, OnInit, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PaginationFormat, SimulationRunDataTableRow } from 'projects/log-viewer-app/src/app/core/interfaces/simulations/simulations-interface';
import { BaseComponent } from 'projects/log-viewer-app/src/app/shared/components/base.component';
import { map } from 'rxjs/operators';

@Component({
  selector: 'eszsw-my-custom-pagination',
  templateUrl: './my-custom-pagination.component.html',
  styleUrls: ['./my-custom-pagination.component.scss']
})
export class MyCustomPaginationComponent extends BaseComponent implements OnInit, AfterViewInit {
  public itemPerPageForm: FormGroup;
  public paginationFormat: PaginationFormat;
  public numberOfPagesArray: number[] = [];
  public showPagination: boolean = false;
  @Input() itemsPerPage: number = 5;
  @Output() onFilterArray: EventEmitter<void> = new EventEmitter<void>();
  @Output() onApplyPaginationFilter: EventEmitter<PaginationFormat> = new EventEmitter<PaginationFormat>();
  constructor() {
    super();
   }

  ngOnInit(): void {
    this.setPaginationFormat();
    this.setItemPerPageForm();
  }
  ngAfterViewInit(): void {
    this.filterArray();
  }

  setPaginationFormat(): void {
    this.paginationFormat = {
      numberOfPages: 0,
      itemsPerPage: this.itemsPerPage,
      currentPage: 0
    };
  }
  setItemPerPageForm() {
    this.itemPerPageForm = new FormGroup({
      itemPerPage: new FormControl(this.itemsPerPage, Validators.required)
    });
    this.subscriptions.push(this.itemPerPageForm.valueChanges.pipe(
      map( (changes: any) => changes.itemPerPage))
      .subscribe( (newItemsPerPage: string) => {
      this.paginationFormat.itemsPerPage = Number(newItemsPerPage);
      this.paginationFormat.currentPage = 0;
      this.filterArray();
    }));
  }

  setPagination(filteredArray: SimulationRunDataTableRow[]) {
    if (filteredArray && filteredArray.length > 0) {
      this.showPagination = true;
    }
    const totalOfPages: number = Math.ceil(filteredArray.length / this.paginationFormat.itemsPerPage);
    this.numberOfPagesArray = Array(totalOfPages).fill(totalOfPages).map((x,i)=>i);
    this.paginationFormat.numberOfPages = totalOfPages;
    this.applyPaginationFilter();
  }

  displayPage(pageNumber: number) {
    this.paginationFormat.currentPage = pageNumber;
    this.filterArray();
  }
  filterArray() {
    this.onFilterArray.emit();
  }

  applyPaginationFilter() {
    this.onApplyPaginationFilter.emit(this.paginationFormat);
  }

}
