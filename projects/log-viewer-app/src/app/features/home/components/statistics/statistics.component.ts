import { DoughnutChartDataSource } from './../../../../core/interfaces/charts/charts-interface';
import { Component, Input, OnInit } from '@angular/core';
import { AppConstants } from 'projects/log-viewer-app/src/app/core/constants/constants';
import { FilterSimulationTable, SimulationRunDataTableRow } from 'projects/log-viewer-app/src/app/core/interfaces/simulations/simulations-interface';

@Component({
  selector: 'eszsw-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  // percentage of runs that exceed the maximum number of stops
  public maxStopNumberRunsExcededTitle: string = '';
  public maxStopNumberRunsExcededPercentage: number;
  public maxStopNumberDataSource: DoughnutChartDataSource[] = [];

  // percentage of runs that exceed the maximum running time
  public maxRunningTimeRunsExcededTitle: string = '';
  public maxRunningTimeRunsExcededPercentage: number;
  public maxRunningDataSource: DoughnutChartDataSource[] = [];

  // percentage of runs that have a collision
  public collisionedRunsTitle: string = '';
  public collisionedRunsPercentage: number;
  public collisionedRunsDataSource: DoughnutChartDataSource[] = [];

  // percentage of runs that do not pass
  public unsuccessfulRunsTitle: string = '';
  public unsuccessfulRunsPercentage: number;
  public unsuccessfulRunsDataSource: DoughnutChartDataSource[] = [];


  private _filteredArray: SimulationRunDataTableRow[] = [];
  @Input() set filteredArray(array: SimulationRunDataTableRow[]) {
    this._filteredArray = array;
    this.calculateStatistics();
  }
  get filteredArray(): SimulationRunDataTableRow[] {
      return this._filteredArray;
  }
   constructor() { }

  ngOnInit(): void {
    this.setChartsTitles();
    this.calculateStatistics();
  }

  setChartsTitles() {
    this.maxStopNumberRunsExcededTitle = 'Percentage of runs that exceed the maximum number of stops';
    this.maxRunningTimeRunsExcededTitle = 'Percentage of runs that exceed the maximum running time';
    this.collisionedRunsTitle = 'Percentage of runs that have a collision';
    this.unsuccessfulRunsTitle = 'Percentage of runs that do not pass';
  }

  calculateStatistics(): void {
    if (this.filteredArray && this.filteredArray.length > 0) {
      this.maxStopNumberRunsExcededPercentage = this.getMaxStopNumberRunsExcededPercentage();
      this.maxRunningTimeRunsExcededPercentage = this.getMaxRunningTimeRunsExceded();
      this.collisionedRunsPercentage = this.getCollisionedRunsPercentage();
      this.unsuccessfulRunsPercentage = this.getUnsuccessfulRunsPercentage();

      this.setCharts();
    }
  }

  getMaxStopNumberRunsExcededPercentage(): number {
    let percentage: number = 0;
    const totalResults: number = this.filteredArray.length;
    const maxStopNumberRunsExceded: number =  this.filteredArray.filter( (simulation: SimulationRunDataTableRow) => simulation.numberOfStops > simulation.maxNumberOfStops).length;
    percentage = (maxStopNumberRunsExceded * AppConstants.MAX_PERCENTAGE) / totalResults;
    this.maxStopNumberDataSource = [
      {
        key: 'Stops exceeded',
        val: (maxStopNumberRunsExceded),
      },
      {
        key: 'Stops not exceeded',
        val: (totalResults - maxStopNumberRunsExceded),
      },
    ];
    return Math.round(percentage);
  }
  getMaxRunningTimeRunsExceded(): number {
    let percentage: number = 0;
    const totalResults: number = this.filteredArray.length;
    const maxRunningTimeRunsExceded: number =  this.filteredArray.filter( (simulation: SimulationRunDataTableRow) => simulation.runningTime > simulation.maxRunningTime).length;
    percentage = (maxRunningTimeRunsExceded * AppConstants.MAX_PERCENTAGE) / totalResults;
    this.maxRunningDataSource = [
      {
        key: 'Time exceeded',
        val: (maxRunningTimeRunsExceded),
      },
      {
        key: 'Time not exceeded',
        val: (totalResults - maxRunningTimeRunsExceded),
      }
    ];
    return Math.round(percentage);
  }
  getCollisionedRunsPercentage(): number {
    let percentage: number = 0;
    const totalResults: number = this.filteredArray.length;
    const maxCollisionsRuns: number =  this.filteredArray.filter( (simulation: SimulationRunDataTableRow) => simulation.hasCollision).length;
    percentage = (maxCollisionsRuns * AppConstants.MAX_PERCENTAGE) / totalResults;
    this.collisionedRunsDataSource = [
      {
        key: 'Collisioned runs',
        val: (maxCollisionsRuns),
      },
      {
        key: 'Not collisioned runs',
        val: (totalResults - maxCollisionsRuns),
      } 
    ];
    return Math.round(percentage);
  }
  getUnsuccessfulRunsPercentage(): number {
    let percentage: number = 0;
    const totalResults: number = this.filteredArray.length;
    const maxUnsuccessfulRuns: number =  this.filteredArray.filter( (simulation: SimulationRunDataTableRow) => !simulation.doesScenarioPass).length;
    percentage = (maxUnsuccessfulRuns * AppConstants.MAX_PERCENTAGE) / totalResults;
    this.unsuccessfulRunsDataSource = [
      {
        key: 'Failed runs',
        val: (maxUnsuccessfulRuns),
      },
      {
        key: 'Successful runs',
        val: (totalResults - maxUnsuccessfulRuns),
      }
    ];
    return Math.round(percentage);
  }

  setCharts() {

  }


}
