import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FilterSimulationTable } from 'projects/log-viewer-app/src/app/core/interfaces/simulations/simulations-interface';

@Component({
  selector: 'eszsw-simulations-table-filters',
  templateUrl: './simulations-table-filters.component.html',
  styleUrls: ['./simulations-table-filters.component.scss']
})
export class SimulationsTableFiltersComponent implements OnInit {

  public filtersForm: FormGroup;
  public filtersResult: FilterSimulationTable;
  @Output() onFiltersApplied: EventEmitter<FilterSimulationTable | void> = new EventEmitter<FilterSimulationTable | void>();
  constructor() { }

  ngOnInit(): void {
    this.setFiltersForm();
  }

  setFiltersForm() {
    this.filtersForm = new FormGroup({
      scenario: new FormControl('', []),
      carBuild: new FormControl('', [])
    });
  }
  onSubmitFilters() {
    console.log('Emilio form: ', this.filtersForm);
    const filtersApplied: FilterSimulationTable = this.filtersForm.value;

    if (filtersApplied && (filtersApplied.scenario !== '' || filtersApplied.carBuild !== '')) {
      this.onFiltersApplied.emit(this.filtersForm.value);
    } else {
      this.onFiltersApplied.emit();
    }
  }
  clearFilters() {
    const clearedFilters: FilterSimulationTable = {scenario: '', carBuild: ''};
    this.filtersForm.reset(clearedFilters);
    this.onFiltersApplied.emit();
  }

}
