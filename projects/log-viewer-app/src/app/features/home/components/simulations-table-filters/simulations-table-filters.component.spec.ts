import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulationsTableFiltersComponent } from './simulations-table-filters.component';

describe('SimulationsTableFiltersComponent', () => {
  let component: SimulationsTableFiltersComponent;
  let fixture: ComponentFixture<SimulationsTableFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationsTableFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulationsTableFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
