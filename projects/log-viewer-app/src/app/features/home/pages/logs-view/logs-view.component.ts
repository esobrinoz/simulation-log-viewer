import { SearchPipe } from './../../../../shared/pipes/search.pipe';
import { FilterSimulationTable, Scenario, Simulation, SimulationRun, SimulationRunDataTableHeader, SimulationRunDataTableRow, SortTableKey, SortTableType } from './../../../../core/interfaces/simulations/simulations-interface';
import { Component, OnInit } from '@angular/core';
import { SimulationsService } from 'projects/log-viewer-app/src/app/core/services/simulations-service/simulations.service';
import { BaseComponent } from 'projects/log-viewer-app/src/app/shared/components/base.component';

@Component({
  selector: 'eszsw-logs-view',
  templateUrl: './logs-view.component.html',
  styleUrls: ['./logs-view.component.scss'],
  providers: [SearchPipe]
})
export class LogsViewComponent extends BaseComponent implements OnInit {

  public simulations: SimulationRun[] = [];
  public scenarios: Scenario[] = [];
  public simulationsDataTableHeaders: SimulationRunDataTableHeader[] = [];
  public simulationsDataTableRows: SimulationRunDataTableRow[] = [];
  public simulationsDataTableFiltered: SimulationRunDataTableRow[] = [];
  public tableFilters: FilterSimulationTable;

  constructor(private simulationService: SimulationsService, private searchByPipe: SearchPipe ) { 
    super();
  }

  ngOnInit(): void {
    this.initialize();
    this.getSimulations();
  }

  initialize() {
    this.setTableHeaders();
  }

  public getSimulations(): void {
    this.subscriptions.push(this.simulationService.getSimulations().pipe(
      // Here uou can add filters or transform the data as you wish
      // filter( (simulations: Simulation) => (simulations.simulationRuns.length > 0 && simulations.scenarios.length > 0))
    ).subscribe(
      (simulations: Simulation) => {
        this.simulations = simulations.simulationRuns;
        this.scenarios = simulations.scenarios;
        this.processData();
      },
      error => console.log(error)
    ));
  }

  processData() {
    const auxTableRows: SimulationRunDataTableRow[] = [];
    this.simulations.forEach( (simulationRun: SimulationRun) => {
      const runningTimeAux: number = this.getMaxRunningTime(simulationRun.scenarioId);
      const maxRunningTimeAux: number = this.calculateMaxRunningTime(simulationRun);
      const maxNumberOfStopsAux: number = this.getMaxNumberOfStops(simulationRun.scenarioId);
      const simulationDataTableRowAux: SimulationRunDataTableRow = {
        scenarioId: simulationRun.scenarioId,
        carBuild: simulationRun.carBuild,
        startTime: simulationRun.startTime,
        runningTime: runningTimeAux,
        maxRunningTime: maxRunningTimeAux,
        numberOfStops: simulationRun.result.numberOfStops,
        maxNumberOfStops: maxNumberOfStopsAux,
        hasCollision: simulationRun.result.hasCollision,
        doesScenarioPass: false
      };
      if (this.doesScenarioPass(simulationDataTableRowAux)) {
        simulationDataTableRowAux.doesScenarioPass = true;
      }
      auxTableRows.push(simulationDataTableRowAux);
    });
    // To fire change detection of @Input inside of simulation-table components
    this.simulationsDataTableRows = [...auxTableRows];
  }

  setTableHeaders() {
    this.simulationsDataTableHeaders = [
      {
        key: SortTableKey.SCENARIO,
        value: 'Scenario ID',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.CAR_BUILD,
        value: 'Car build',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.START_TIME,
        value: 'Start time',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.RUNNING_TIME,
        value: 'Running time / Max running time',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.NUMBER_OF_STOPS,
        value: 'Number of stops / Max number of stops',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.HAS_COLLISION,
        value: 'Has collision',
        sortType: SortTableType.ASCENDENT
      },
      {
        key: SortTableKey.DOES_SCENARIOS_PASS,
        value: 'Scenario successful',
        sortType: SortTableType.ASCENDENT
      }
    ];

  }

  getMaxRunningTime(scenarioId: string): number {
    let maxRunningTime: number = -1;
    const scenarioIndex: number = this.scenarios.findIndex( (scenarioAux: Scenario) => scenarioAux.scenarioId === scenarioId);
    if (scenarioIndex !== -1) {
      const scenarioAux: Scenario = this.scenarios[scenarioIndex];
      maxRunningTime = scenarioAux.maxRunningTime;
    }
    return maxRunningTime;
  }
  getMaxNumberOfStops(scenarioId: string): number {
    let maxNumberOfStops: number = -1;
    const scenarioIndex: number = this.scenarios.findIndex( (scenarioAux: Scenario) => scenarioAux.scenarioId === scenarioId);
    if (scenarioIndex !== -1) {
      const scenarioAux: Scenario = this.scenarios[scenarioIndex];
      maxNumberOfStops = scenarioAux.maxNumberOfStops;

    }
    return maxNumberOfStops;
  }

  doesScenarioPass(simulation: SimulationRunDataTableRow): boolean {
    let scenarioSuccessful: boolean = false;
    if (simulation.numberOfStops <= simulation.maxNumberOfStops &&
       simulation.runningTime <= simulation.maxRunningTime && 
       !simulation.hasCollision) {
      scenarioSuccessful = true;
    }
    return scenarioSuccessful;
  }
  calculateMaxRunningTime(simulationRun: SimulationRun): number {
    return Math.abs(simulationRun.endTime - simulationRun.startTime);
  }

  applyFilters(filters: FilterSimulationTable) {
    this.tableFilters = filters;
    if (filters) {
      this.simulationsDataTableFiltered = this.searchByPipe.transform(this.simulationsDataTableRows, filters);
    } else {
      this.simulationsDataTableFiltered = [];
    }
  }

}
