import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {MatTabsModule} from '@angular/material/tabs';
import { LogsViewComponent } from './pages/logs-view/logs-view.component';
import { SimulationsTableFiltersComponent } from './components/simulations-table-filters/simulations-table-filters.component';
import { StatisticsComponent } from './components/statistics/statistics.component';

@NgModule({
  declarations: [
    HomeComponent,
    LogsViewComponent,
    SimulationsTableFiltersComponent,
    StatisticsComponent
  ],
  imports: [
    SharedModule,
    HomeRoutingModule,
    MatTabsModule
  ]
})
export class HomeModule { }
