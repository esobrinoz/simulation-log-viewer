import { LogsViewComponent } from './pages/logs-view/logs-view.component';
import { RoutePath } from './../../core/enums/route.paths';
import { RouterModule, Routes } from '@angular/router';

const homeRoutes: Routes = [
    {
      path: '',
      component: LogsViewComponent
    },
    {
      path: RoutePath.LOGS_VIEW,
      component: LogsViewComponent
    } 
];


export const HOME_ROUTES = RouterModule.forChild(homeRoutes);
