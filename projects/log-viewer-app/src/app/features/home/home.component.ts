import { Subscription } from 'rxjs';
import { RoutePath } from './../../core/enums/route.paths';
import { Component, ViewEncapsulation, OnInit, Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'eszsw-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  public subscription: Subscription[];
  public logsView: string;
  public context: string;

  constructor(private renderer: Renderer2, @Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
    this.initialize();
    this.addHomeClass();
  }

  public initialize(): void {
    this.logsView = RoutePath.HOME; 
  }

  public addHomeClass ():void{
    if(this.document.getElementById('body')) {
      this.renderer.addClass(this.document.getElementById('body'), 'home');
    }
  }

}
